# Python3.6 + django2 + nginx + gunicorn + postgresql

## Introduction

Django2 sample project


## Precondition

`django2-nginx.json`, `django2-nginx-postgresql.json` needs:

`python3.6-nginx:latest` docker image on your project, see [https://gitlab.com/imp555/python3.6-nginx.git](https://gitlab.com/imp555/python3.6-nginx.git)


`django2-nginx-rhel7.json`, `django2-nginx-postgresql-rhel7.json` needs:

`python3.6-nginx-rhel7:latest` docker image on your project, see [https://gitlab.com/imp555/python3.6-nginx-rhel7.git](https://gitlab.com/imp555/python3.6-nginx-rhel7.git)


`django2-nginx-postgresql-rhel7.json` needs:

[registry.access.redhat.com/rhscl/postgresql-96-rhel7:latest](https://access.redhat.com/containers/?tab=overview#/registry.access.redhat.com/rhscl/postgresql-96-rhel7) docker image on your project.

```
oc import-image postgresql-96-rhel7:latest --from=registry.access.redhat.com/rhscl/postgresql-96-rhel7 --confirm
```

## Usage 

1. create base docker image on your project
   `python3.6-nginx:latest` or `python3.6-nginx-rhel7:latest`
2. `Import YAML/JSON` on your OpenShift WebConsole

   `django2-nginx.json` ... django2 sample project with SQLLite3 (centos base)
   `django2-nginx-postgresql.json` ... django2 sample project with PostgreSQL (centos base)
   `django2-nginx-rhel7.json` ... django2 sample project with SQLLite3 (rhscl/python-36-rhel7 base)
   `django2-nginx-postgresql-rhel7.json` ... django2 sample project with Potgresql (rhscl/python-36-rhel7 base)
3. wait for building image stream (labeled `django2-nginx-ex:latest`)
4. access to url displayed on `Overview` console (ex. http://django2-nginx-ex-django2-nginx.192.168.42.246.nip.io/)
5. change access path to `/api/` (ex. http://django2-nginx-ex-django2-nginx.192.168.42.246.nip.io/api/)


## Special files in this repository

### base files

```
conf/              - gunicorn config file
etc/
  nginx/           - nginx config files
openshift/         - OpenShift-specific files
  scripts          - helper scripts
  templates        - application templates
requirements.txt   - list of dependencies
```

### Django2 files


```
restapi/           - Django2 sample project application files
sample/            - Django2 sample project core files
static/            - Django2 static files
manage.py          - Django2 sample project's manage.py
wsgi.py            - Django2 sample project's wsgi.py (copy from sample project core files folder)
```

## Note

use your Django2 project, replace `Django2 files` and rebuild.
`static/` folder is necessary.

- about Django2 sample project, see https://qiita.com/imp555sti/items/cbe579fbbfa3837075f3(Japanese)
- about to use your Django2 project, see https://qiita.com/imp555sti/items/7733b48f41fc37ddda24(Japanese)
