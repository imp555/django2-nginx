from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

# Create your tests here.
class restapiTest(APITestCase):
    def test_current_time(self):
        url = reverse('current_time')
        response = self.client.get(url, '')

        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_health(self):
        url = reverse('health')
        response = self.client.get(url, '')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
