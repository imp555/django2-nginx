from datetime import datetime
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view

# Create your views here.

@api_view(['GET'])
def current_time(request):
    ret = {'now':str(datetime.now())}
    return JsonResponse(ret, status=status.HTTP_200_OK)

def health(request):
    return HttpResponse('')
