"""
You can set APP_CONFIG to point to this file to enable automatic reloading of
modules.
"""

# ===============================================================================
# gunicorn settings
# http://docs.gunicorn.org/en/stable/settings.html
# ===============================================================================

# ------------------------------------------------------------------------------
# Debugging
# ------------------------------------------------------------------------------

reload = True

# ------------------------------------------------------------------------------
# Logging
# ------------------------------------------------------------------------------

# gunicorn original access log format
# access_log_format = '%(h)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'

access_log_format = '%(h)s %(l)s %(u)s %(t)s %({Host}i)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s" %({X-Forwarded-For}i)s %(l)s %(b)s %(D)s %(l)s'

# gunicorn error log to stderr
errorlog = '-'


# ------------------------------------------------------------------------------
# Worker Processes
# ------------------------------------------------------------------------------

timeout = 30
keepalive = 3



